
#include <LiquidCrystal.h>


#define DIRECT A0
#define RETOUR A1
#define MOTEUR A2

LiquidCrystal lcd(12, 11, 6, 5, 4, 3);



//declare variables for the motor pins
#define motorPin1 7     // Blue   - 28BYJ48 pin 1
#define motorPin2 8     // Pink   - 28BYJ48 pin 2
#define motorPin3 9     // Yellow - 28BYJ48 pin 3
#define motorPin4 10    // Orange - 28BYJ48 pin 4
// Red    - 28BYJ48 pin 5 (VCC)
#define INTER 2

int motorSpeed = 1000; //1200  //variable to set stepper speed
//int count = 0;          // count of steps made
//int countsperrev = 512; // number of steps per full revolution
int pas_entier[8] = {B01000, B01100, B00100, B00110, B00010, B00011, B00001, B01001};
int lookup[4] = {B01000, B00100, B00010, B00001};

bool verrou = true;
bool pas = true;

int Analog_dir = 0;
int Analog_ret = 0;
double Pui_dir = 0.0;
double Pui_ret = 0.0;
double RL = 1.0;
double Pui_dir_old = 0.0;
double Pui_ret_old = 0.0;
double micro_old;
double micro;
int temps = 0;
int temps_old = 0;



void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

  lcd.begin(20, 4);
  lcd.setCursor(0, 0);
  lcd.print("F4HDG | ROSmatic");
  //lcd.setCursor(0,1);
  //lcd.print("ROSmatic");

  lcd.setCursor(10, 2);
  lcd.print("Moteur");
  lcd.setCursor(10, 3);
  lcd.print("  Bloc");

  //declare the motor pins as outputs
  pinMode(motorPin1, OUTPUT);
  pinMode(motorPin2, OUTPUT);
  pinMode(motorPin3, OUTPUT);
  pinMode(motorPin4, OUTPUT);

  Serial.begin(9600);

  pinMode(INTER, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTER), verrouillage, RISING);



  //delay(1000);

}

void loop() {
  int i = 0;
  int Analog_mot;

  micro  = micros();

  // read the analog in value:
  Analog_dir = analogRead(DIRECT);
  Analog_ret = analogRead(RETOUR);
  Analog_mot = analogRead(MOTEUR);

  for (i = 0; i < 10; i++)
  {
    Analog_dir = (Analog_dir + analogRead(DIRECT)) / 2;
    Analog_ret = (Analog_ret + analogRead(RETOUR)) / 2;
    Analog_mot = (Analog_mot + analogRead(MOTEUR)) / 2;

  }

  // map it to the range of the analog out:
  Pui_dir = (Analog_dir * 5.0) / 1023.0;
  Pui_ret = (Analog_ret * 5.0) / 1023.0;

/*
Serial.print("Micro");
  Serial.println(micro);
  Serial.print("Micro old");
  Serial.println(micro_old);
  Serial.print("diff");
  Serial.println(micro - micro_old);
*/

  if ((abs(Pui_dir - Pui_dir_old) > 0.02 || abs(Pui_ret - Pui_ret_old) > 0.02) && (micro - micro_old) > 200e3)
  {

    //RL = sqrt(Pui_ret/Pui_dir);
    RL = Pui_ret / Pui_dir;
    RL = (1 + RL) / (1 - RL);

    lcd.setCursor(0, 1);
    lcd.print("ROS ");
    lcd.print(RL);
    lcd.setCursor(0, 2);
    lcd.print("Dir ");
    lcd.print(Pui_dir);
    lcd.setCursor(0, 3);
    lcd.print("Ref ");
    lcd.print(Pui_ret);
    micro_old = micro;
  }
  // Delay max = (440 * 31 + 2000 = 15640
  // Delay min = 2000

  if (!verrou)
  {
    do
    {
      if (Analog_mot > 582)
      {
        motorSpeed = 15640 - (Analog_mot - 582) * 31; //16000;
        clockwise();
      }
      else if (Analog_mot < 440)
      {
        motorSpeed = Analog_mot * 31 + 2000; //16000;
        anticlockwise();
      }
      //else
       
              //setOutputOFF();
    }
    while (Analog_mot < 440 && Analog_mot > 582);
  }
  //delay(500);
  Pui_dir_old = Pui_dir;
  Pui_ret_old = Pui_ret;


}


void verrouillage()
{
  temps = millis();
  if ((temps - temps_old) > 500)
  {
    temps_old = temps;
    verrou = !verrou;
    lcd.setCursor(10, 3);
    if (verrou)
      lcd.print("  Bloc");
    else
      lcd.print("Debloc");
  }
}


//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 1 to 4
//delay "motorSpeed" between each pin setting (to determine speed)
void anticlockwise()
{
  //for (int i = 0; i < 8; i++)
  for (int i = 0; i < 4; i++)
  {
    setOutput(i);
    delayMicroseconds(motorSpeed);
    //delay(1000);
  }

}

void clockwise()
{
//  for (int i = 8; i > 0; i--)
  for (int i = 4; i > 0; i--)
    //for (int i = 7; i >= 0; i--)
  {
    setOutput(i);
    delayMicroseconds(motorSpeed);
    //delay(1000);
  }
}

void setOutput(int out)
{
  digitalWrite(motorPin1, bitRead(lookup[out], 0));
  digitalWrite(motorPin2, bitRead(lookup[out], 1));
  digitalWrite(motorPin3, bitRead(lookup[out], 2));
  digitalWrite(motorPin4, bitRead(lookup[out], 3));
}

void setOutputOFF()
{
  digitalWrite(motorPin1, 0);
  digitalWrite(motorPin2, 0);
  digitalWrite(motorPin3, 0);
  digitalWrite(motorPin4, 0);
}
